# Game Engine Programming C++ #

This repo was made to keep track of my progress while following *Game Programming in C++* by Sanjay Madhav. He uses SDL and OpenGL as a basis to teach the fundamentals of Game Engine programming. All code is my own. Within each chapter, are variants of the code for each of the comprehensive exercises.

### How do I get set up? ###

To compile on Windows, install Microsoft Visual Studio Community
(https://www.visualstudio.com/downloads/). During installation, select the
"Game Development in C++" workflow. In each Chapter directory, there is a
corresponding ChapterXX-windows.sln file to open.

Code for Chapter 7 and beyond uses the FMOD API for audio. This requires
a separate installation from (https://www.fmod.com/download). Download
and install version 1.09.x of the FMOD Studio API (newer versions are untested).
On Windows, install FMOD to the default directory. On Mac, copy the contents
of the FMOD package into External/FMOD.

### Check out my portfolio below for more ###

(https://www.zared.co)